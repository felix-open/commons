package me.tony.open.commons.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Enumeration;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Created by youqi on 2016/11/27 14:38.
 * 文件工具类
 */
public class FileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    /**
     * 删除文件或者文件夹，忽略任何异常
     * 底层调用commons-io的实现
     *
     * @param file 文件路径
     * @return 返回删除结果
     */
    public static boolean delete(String file) {
        return org.apache.commons.io.FileUtils.deleteQuietly(new File(file));
    }

    /**
     * 移动文件或者文件夹
     *
     * @param src    原文件
     * @param target 目标路径文件
     * @see org.apache.commons.io.FileUtils
     */
    public static void move(String src, String target) {
        File srcFile = new File(src);
        File targetFile = new File(target);
        try {
            if (srcFile.isDirectory())
                org.apache.commons.io.FileUtils.moveDirectory(srcFile, targetFile);
            else
                org.apache.commons.io.FileUtils.moveFile(srcFile, targetFile);

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * 解压缩文件
     *
     * @param zipFile        zip文件
     * @param outputFolder   输出目录
     * @param newNamePreffix 新添加的前缀，null表示保留原名称
     */
    public static void unzip(File zipFile, String outputFolder, String newNamePreffix) {

        try (ZipFile zip = new ZipFile(zipFile)) {
            Enumeration<? extends ZipEntry> entries = zip.entries();

            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = entries.nextElement();

                InputStream is = zip.getInputStream(zipEntry);
                String filePath;
                if (newNamePreffix != null) {
                    filePath = outputFolder + File.separator + newNamePreffix + zipEntry.getName();
                } else {
                    filePath = outputFolder + File.separator + zipEntry.getName();
                }

                org.apache.commons.io.FileUtils.copyInputStreamToFile(is, new File(filePath));
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * 对文件进行压缩
     *
     * @param sourcePath 文件或者目录
     * @param zipPath    目标文件
     */
    public static void zip(String sourcePath, String zipPath) {
        try (FileOutputStream fos = new FileOutputStream(zipPath);
             ZipOutputStream zos = new ZipOutputStream(fos)) {
            zip(new File(sourcePath), "", zos);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private static void zip(File file, String parentPath, ZipOutputStream zos) {
        if (file.isDirectory()) {
            //文件夹
            parentPath += file.getName() + File.separator;

            String finalParentPath = parentPath;
            File[] files = file.listFiles();
            if (files != null) {
                Stream.of(files).forEach(one -> zip(one, finalParentPath, zos));
            }
        } else {
            ZipEntry ze = new ZipEntry(parentPath + file.getName());
            try (FileInputStream fis = new FileInputStream(file)) {
                zos.putNextEntry(ze);
                byte[] content = new byte[1024 * 1024 * 2];
                int len;
                while ((len = fis.read(content)) != -1) {
                    zos.write(content, 0, len);
                    zos.flush();
                }
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }
}
